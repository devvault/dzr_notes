class CfgPatches
{
	class dzr_notes_on_object
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_notes_on_object
	{
		type = "mod";
		author = "DayZRussia.com";
		description = "Add a text note to any object";
		dir = "dzr_notes_on_object";
		name = "DZR Notes on Objects";
		inputs = "dzr_notes_on_object/Data/Inputs.xml";
		dependencies[] = {"Game", "World", "Mission"};
		class defs
		{
			class gameScriptModule
			{
				files[] = {"dzr_notes_on_object/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_notes_on_object/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_notes_on_object/5_Mission"};
			};

		};
	};
};