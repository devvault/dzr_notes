class WriteOnObject2: ActionInteractBase
{
	
	void WriteOnObject2()
	{
		//m_CallbackClass = ActionWritePaperCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONFB_VIEWNOTE;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_CROUCH | DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_PRONE;
	}

	override string GetText()
	{
		return "2 Добавить надпись";
	}

	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		return true;
	}

	override void OnExecuteServer( ActionData action_data )
	{
Print("WriteOnObject2: Server");
	}
	
	
	override void OnExecuteClient( ActionData action_data )
	{
Print("WriteOnObject2: Client");
	}
	
	override void OnEndServer( ActionData action_data )
	{
	
	Print("WriteOnObject2: OnEndServer");
		/*
		PlayerBase target_player = PlayerBase.Cast(action_data.m_Target.GetObject());
		PlayerBase player = action_data.m_Player;
		CachedObjectsParams.PARAM1_INT.param1 = target_player.GetStatLevelBlood();
		GetGame().RPCSingleParam( target_player ,ERPCs.RPC_CHECK_PULSE, CachedObjectsParams.PARAM1_INT, true, target_player.GetIdentity() );
		*/
	}
}