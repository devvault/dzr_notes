class ReadObjectNote: ActionInteractBase
{
	bool m_ShowAction;
	bool m_CalledDelayedShow;
	
	void ReadObjectNote()
	{
		//GetGame().Chat("Действие добавлено", "colorImportant" );
		m_ShowAction = false;
		m_CalledDelayedShow = false;
		//m_CallbackClass = WriteOnObjectCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONMOD_OPENDOORFW;
		m_FullBody = true;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_CROUCH | DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_PRONE;
	}
	
	override void CreateConditionComponents()
	{	
		m_ConditionItem = new CCINonRuined;
		m_ConditionTarget = new CCTCursor;
	}
	
	override string GetText()
	{
		return "Прочитать надпись";
	}
	
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		//GetGame().Chat("Проверка действия чтения", "colorImportant" );
		if(!m_CalledDelayedShow){
			//GetGame().Chat("!m_CalledDelayedShow", "colorImportant" );
			GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.DelayedShow, 4000, true);
			m_CalledDelayedShow = true;
		} 
		
		if(!m_ShowAction)
		{
			//GetGame().Chat("!m_ShowAction", "colorImportant" );
			return false; 
		}
		else {
			//GetGame().Chat("m_ShowAction", "colorImportant" );
			if( !target ) return false;
			//if( IsDamageDestroyed(action_data.m_Target) ) return false;
			//if( !IsBuilding(target) ) return false;
			if( !IsInReach(player, target, UAMaxDistances.DEFAULT) ) return false;
			
			Building building;
			if( Class.CastTo(building, target.GetObject()) )
			{
				int doorIndex = building.GetDoorIndex(target.GetComponentIndex());
				if ( doorIndex != -1 ){
					return true;
				}
			}		
		};
		return true;
	}
	
	override void OnStartClient(ActionData action_data)
	{
		GetGame().Chat("START READING TEXT", "colorImportant" );
		/*
			if (action_data.m_Target.GetObject().ConfigIsExisting("rgbColor"))
			action_data.m_MainItem.GetWrittenNoteData().InitNoteInfo(ItemBase.Cast(action_data.m_Target.GetObject()), action_data.m_MainItem);
			
			else
			ItemBase.Cast(action_data.m_Target.GetObject()).GetWrittenNoteData().InitNoteInfo(action_data.m_MainItem, ItemBase.Cast(action_data.m_Target.GetObject()));
		*/
	}
	/*
		override void OnUpdate(ActionData action_data)
		{
		super.OnUpdate(action_data);
		
		if(!GetGame().IsMultiplayer() || GetGame().IsClient())
		{
		if (action_data.m_State == UA_FINISHED && GetGame().GetUIManager() && !GetGame().GetUIManager().IsMenuOpen(MENU_NOTE))
		{
		ActionManagerClient.Cast(action_data.m_Player.GetActionManager()).RequestEndAction();
		}
		}
		
		}
	*/
	
	void DelayedShow()
	{
		GetGame().Chat("Прошло 4 секунды", "colorImportant" );
		m_ShowAction = true;
	}
	
	override void OnExecuteServer( ActionData action_data )
	{
		Print("Чтение выполнено");
		GetGame().Chat("READING NOTE", "colorImportant" );
		//GetGame().Chat("OnExecuteServer", "colorImportant" );
		//string objectClass = action_data.m_Target.GetObject().GetType();
		//AddWriting(action_data.m_Target, objectClass, action_data.m_Player);
	}
};