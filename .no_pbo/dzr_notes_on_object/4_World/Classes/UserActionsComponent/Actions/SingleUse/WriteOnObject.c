class WriteOnObject: ActionSingleUseBase
{
	
	void WriteOnObject()
	{
		
		//m_CallbackClass = WriteOnObjectCB;
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONMOD_ITEM_ON;
		m_FullBody = false;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_CROUCH | DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_PRONE;
	}
	/*	
		override void CreateConditionComponents()
		{	
		m_ConditionItem = new CCINone;
		m_ConditionTarget = new CCTCursor; //CCTCursorParent(UAMaxDistances.DEFAULT);
		}
	*/
	override string GetText()
	{
		return "Подписать";
	}
	
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		if( !target ) return false;
		//if( IsDamageDestroyed(action_data.m_Target) ) return false;
		//if( !IsBuilding(target) ) return false;
		if( !IsInReach(player, target, UAMaxDistances.DEFAULT) ) return false;
		
		Building building;
		if( Class.CastTo(building, target.GetObject()) )
		{
			int doorIndex = building.GetDoorIndex(target.GetComponentIndex());
			if ( doorIndex != -1 ){
				return true;
			}
		}		
		return true;
	}
	
	protected void AddWriting(ActionTarget target, string objectClass, PlayerBase player)
	{
		
		GetGame().Chat("AddWriting", "colorImportant" );
		int b1 = 0;
		int b2 = 0;
		int b3 = 0;
		int b4 = 0;
		
		if (target)
		{
			EntityAI target_object = EntityAI.Cast( target.GetObject());
			if(target_object)
			{
				target_object.GetPersistentID(b1, b2, b3, b4);
			}
			
			string m_PersistenceId = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
			//del
			array<string> strDirtyFilename;
			array<string> strDirtyFilename2;
			strDirtyFilename = new array<string>;
			strDirtyFilename2 = new array<string>;
			string playerName = player.GetIdentity().GetName();
			Print(playerName + " подписал предмет "+target);
			string StringedObjectName = target.ToString();
			Print("Stringified Object: "+StringedObjectName);
			StringedObjectName.Split( "<", strDirtyFilename );
			strDirtyFilename.Get(1).Split( ">", strDirtyFilename2 );
			string objectID = strDirtyFilename2.Get(0);
			string ObjectCleanName = objectClass+"_"+m_PersistenceId; //TODO SPLIT <> grab numbers
			//del
			
			string m_ObjectFileName = "$profile:DZR/dzr_notes/"+ObjectCleanName+".txt";
			FileHandle fhandle;
			if ( !FileExist(m_ObjectFileName) )
			{
				fhandle	=	OpenFile(m_ObjectFileName, FileMode.WRITE);
				FPrintln(fhandle, playerName);
				CloseFile(fhandle);
				Print("Создан файл "+m_ObjectFileName+" с текстом: " +playerName);
				} else {
				
				//file
				fhandle	=	OpenFile(m_ObjectFileName, FileMode.APPEND);
				FPrintln(fhandle, "добавлено: "+playerName);
				CloseFile(fhandle);
				//Print("Создан файл "+m_ObjectFileName+" с текстом: " +playerName);
				//file
				Print("Файл "+m_ObjectFileName+" уже существует. Заменяем содержимое на НОВОЕ.");
			}
		}
		
	}
	override void OnStartClient(ActionData action_data)
	{
		GetGame().Chat("START TEXT INPUT", "colorImportant" );
		/*
			if (action_data.m_Target.GetObject().ConfigIsExisting("rgbColor"))
			action_data.m_MainItem.GetWrittenNoteData().InitNoteInfo(ItemBase.Cast(action_data.m_Target.GetObject()), action_data.m_MainItem);
			
			else
			ItemBase.Cast(action_data.m_Target.GetObject()).GetWrittenNoteData().InitNoteInfo(action_data.m_MainItem, ItemBase.Cast(action_data.m_Target.GetObject()));
			*/
		}
		/*
			override void OnUpdate(ActionData action_data)
			{
			super.OnUpdate(action_data);
			
			if(!GetGame().IsMultiplayer() || GetGame().IsClient())
			{
			if (action_data.m_State == UA_FINISHED && GetGame().GetUIManager() && !GetGame().GetUIManager().IsMenuOpen(MENU_NOTE))
			{
			ActionManagerClient.Cast(action_data.m_Player.GetActionManager()).RequestEndAction();
			}
			}
			
			}
		*/
		
		
		override void OnExecuteServer( ActionData action_data )
		{
			Print("Действие выполнено");
			GetGame().Chat("OnExecuteServer", "colorImportant" );
			string objectClass = action_data.m_Target.GetObject().GetType();
			AddWriting(action_data.m_Target, objectClass, action_data.m_Player);
		}
	};		